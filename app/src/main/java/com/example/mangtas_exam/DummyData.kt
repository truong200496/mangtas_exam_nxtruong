package com.example.mangtas_exam

import java.util.*

object DummyData {
    fun getListHoliday(): List<Holiday> {
        return listOf(
            Holiday(Calendar.FEBRUARY, 5, HolidayType.FIXED),
            Holiday(Calendar.NOVEMBER, 23, HolidayType.FIXED),
            Holiday(Calendar.NOVEMBER, 11, HolidayType.FIXED),
            Holiday(Calendar.NOVEMBER, 7, HolidayType.FIXED),
            Holiday(Calendar.NOVEMBER, 16, HolidayType.FIXED),

            Holiday(Calendar.NOVEMBER, 13, HolidayType.WEEKEND),
            Holiday(Calendar.OCTOBER, 3, HolidayType.WEEKEND),
            Holiday(Calendar.OCTOBER, 4, HolidayType.WEEKEND),
            Holiday(Calendar.OCTOBER, 22, HolidayType.WEEKEND),
            Holiday(Calendar.OCTOBER, 29, HolidayType.WEEKEND),

            Holiday(type = HolidayType.DYNAMIC, dynamic = DynamicHoliday(Calendar.MONDAY, 2, Calendar.JUNE)),
        )
    }
}
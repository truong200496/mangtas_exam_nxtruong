package com.example.mangtas_exam

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.example.mangtas_exam.DateCalculator.calculateWorkingDays
import com.example.mangtas_exam.DateCalculator.clearAll
import com.example.mangtas_exam.DateCalculator.dynamicHolidayCount
import com.example.mangtas_exam.DateCalculator.fixDateCount
import com.example.mangtas_exam.DateCalculator.weekendHolidayCount
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.DateValidatorPointForward
import com.google.android.material.datepicker.MaterialDatePicker
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.*
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {

    private var job: Job? = null

    @SuppressLint("SimpleDateFormat")
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val startPicker = MaterialDatePicker.Builder.datePicker()
            .setTitleText(getString(R.string.date_picker_start_title)).build()

        var startDate: Long = 0
        var endDate: Long = 0

        startDateWrapper.setEndIconOnClickListener {
            startPicker.show(supportFragmentManager, "start_date_picker")
        }

        startPicker.addOnPositiveButtonClickListener {
            val dateString: String =
                SimpleDateFormat(DateCalculator.DATE_FORMAT).format(Date(it))
            edtStartDate.setText(dateString)
            startDate = it

            val constraintsBuilder =
                CalendarConstraints.Builder()
                    .setValidator(DateValidatorPointForward.from(startDate))
            val endPicker = MaterialDatePicker.Builder.datePicker()
                .setCalendarConstraints(constraintsBuilder.build())
                .setTitleText(getString(R.string.date_picker_end_title))
                .build()
            endPicker.addOnPositiveButtonClickListener {
                val dateString: String =
                    SimpleDateFormat(DateCalculator.DATE_FORMAT).format(Date(it))
                edtEndDate.setText(dateString)
                endDate = it
            }
            endDateWrapper.setEndIconOnClickListener {
                endPicker.show(supportFragmentManager, "end_date_picker")
            }
        }

        btnCalculate.setOnClickListener {
            job = CoroutineScope(Dispatchers.IO).launch {
                val workingDays = calculateWorkingDays(startDate, endDate)
                withContext(Dispatchers.Main) {
                    updateResult(workingDays)
                    clearAll()
                }
            }
        }
    }

    private fun updateResult(workingDays: Int) {
        tvFixedDate.text = String.format(getString(R.string.fixed_day), fixDateCount)
        tvWeekendHoliday.text =
            String.format(getString(R.string.weekend_holiday), weekendHolidayCount)
        tvDynamicDate.text = String.format(getString(R.string.dynamic_day), dynamicHolidayCount)
        tvWeekDays.text = String.format(getString(R.string.weeks_day), workingDays)
    }

    override fun onDestroy() {
        super.onDestroy()
        job?.cancel()
    }
}

